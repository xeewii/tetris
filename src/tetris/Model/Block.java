package tetris.Model;

import java.awt.*;

public class Block {
    private final int BLOCK_SIZE = 25;
    private int x, y;

    public Block(final int x, final int y){
        setX(x);
        setY(y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() { return y; }

    public int getX() { return x; }

    void paint(Graphics g, int color){
        g.setColor(new Color(color));
        g.drawRoundRect(x*BLOCK_SIZE, y*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, 0,0);
    }
}
