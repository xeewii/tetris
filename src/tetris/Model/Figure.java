package tetris.Model;

import java.awt.*;
import java.util.*;

public class Figure {
    final int[][][]SHAPES = {
            {{0,0,0,0}, {1,1,1,1}, {0,0,0,0}, {0,0,0,0}, {4}}, // I
            {{0,0,0,0}, {0,1,1,0}, {0,1,1,0}, {0,0,0,0}, {4}}, // O
            {{1,0,0,0}, {1,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3}}, // J
            {{0,0,1,0}, {1,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3}}, // L
            {{0,1,1,0}, {1,1,0,0}, {0,0,0,0}, {0,0,0,0}, {3}}, // S
            {{1,1,1,0}, {0,1,0,0}, {0,0,0,0}, {0,0,0,0}, {3}}, // T
            {{1,1,0,0}, {0,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3}}, // Z
    };

    final int[] COLORS = {
            0xF08080, 0xDC143C, 0xFF69B4, 0xFF7F50, 0xF0E68C, 0xDDA0DD, 0x9370DB, 0x800080, 0xD2691E, 0xBC8F8F, 0xA52A2A, 0x8B4513,
            0x98FB98, 0x9ACD32, 0x66CDAA, 0x8FBC8F, 0x7FFFD4, 0x4682B4, 0x7B68EE, 0xD3D3D3, 0x808080, 0x778899, 0x2F4F4F
    };

    private ArrayList<Block> figure = new ArrayList<Block>();
    private int[][] shape = new int[4][4];
    private int color, type, size;
    private int x=3, y=0;

    public Figure(){
        type = new Random().nextInt(SHAPES.length);
        size = SHAPES[type][4][0];
        color = COLORS[new Random().nextInt(COLORS.length)];
        //if (size == 4) y=-1;
        for (int i=0; i<size; i++){
            System.arraycopy(SHAPES[type][i], 0, shape[i], 0, SHAPES[type][i].length);
        }
        createShape();
    }

    public void createShape(){
        for (int x = 0; x < size; x++){
            for (int y=0; y < size; y++){
                if (shape[y][x] == 1){
                    figure.add(new Block(x+this.x, y+this.y));
                }
            }
        }
    }

    public void fallDown(){
        for (Block block : figure) block.setY(block.getY() + 1);
        y++;
    }

    public void paint(Graphics g){
        for(Block block : figure){
            block.paint(g, color);
        }
    }

    public void addX(int dx){
        x += dx;
    }

    public void rotate(int direction){
        for (int i = 0; i < size/2; i++)
            for (int j = i; j < size-1-i; j++)
                if (direction == 1) {
                    int tmp = shape[size-1-j][i];
                    shape[size-1-j][i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[i][j];
                    shape[i][j] = tmp;
                } else {
                    int tmp = shape[i][j];
                    shape[i][j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[size-1-j][i];
                    shape[size-1-j][i] = tmp;
                }
    }

    public void clear() { figure.clear(); }

    public int getValCell(int y, int x) { return shape[y][x]; }
    public int getSize() { return size; }
    public int getColor() { return color; }
    public int getY() { return y; }
    public int getX() { return x; }

    public ArrayList<Block> getFigure() { return figure; }
}
