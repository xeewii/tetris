package tetris.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame {
    private final int HEIGHT = 18;
    private final int WIDTH = 10;
    private final int BLOCK_SIZE = 25;
    private Board board = new Board(HEIGHT, WIDTH, BLOCK_SIZE);

    private JPanel menu = new JPanel();

    private JButton startButton;

    private boolean startGame = false;

    public Frame(){
        setTitle("Tetris");
        setSize(WIDTH*BLOCK_SIZE + 17 , HEIGHT*BLOCK_SIZE + 40);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        startButton = new JButton("start");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                remove(menu);

                board = new Board(HEIGHT, WIDTH, BLOCK_SIZE);
                add(board);

                revalidate();
                repaint();

                startGame = true;
            }
        });

        menu = new JPanel();
        menu.add(startButton);

        add(menu);
        setVisible(true);
    }

    public void start(){
        while(!startGame) {}
        board.start();
    }
}
