package tetris.View;

import tetris.Controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Board extends JPanel {
    private final int HEIGHT;
    private final int WIDTH;
    private final int BLOCK_SIZE;
    private Controller controller;

    private JButton startButton;

    private boolean start = false;

    public Board(int height, int width, int blockSize){
        super();
        HEIGHT = height;
        WIDTH = width;
        BLOCK_SIZE = blockSize;
        setFocusable(true);
        setBackground(Color.black);
        controller = new Controller(HEIGHT, WIDTH, BLOCK_SIZE, this);
        addKeyListener(new MyAdapter());
        setLayout(null);
    }

    public void createMenu(){
        startButton = new JButton("start");
        startButton.setSize(80, 30);
        startButton.setLocation(85,20);
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start = true;
            }
        });
        add(startButton);

    }

    public void start(){
        controller.start();
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
            for (int x = 0; x < WIDTH; x++)
                for (int y = 0; y < HEIGHT; y++) {
                    if (controller.getColor(x, y) > 0) {
                        g.setColor(new Color(controller.getColor(x, y)));
                        g.fill3DRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, true);
                    }
                }

            if (controller.isGameOver()) {

            } else controller.paint(g);
    }

    private class MyAdapter extends KeyAdapter {
        public void keyPressed(KeyEvent event){
            if (!controller.isGameOver()) {
                int code = event.getKeyCode();
                if (code == KeyEvent.VK_DOWN) controller.drop();
                if (code == KeyEvent.VK_UP) controller.rotate();
                if (code == KeyEvent.VK_LEFT || code == KeyEvent.VK_RIGHT) controller.move(code);
                repaint();
            }
        }
    }
}
