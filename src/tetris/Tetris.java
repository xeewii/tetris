package tetris;

import tetris.View.Frame;

public class Tetris {
    private Frame frame = new Frame();

    public static void main(String[] args) {
        Tetris tetris = new Tetris();
        tetris.start();
    }

    public void start(){
        frame.start();
    }
}
