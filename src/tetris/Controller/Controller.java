package tetris.Controller;

import tetris.Model.Block;
import tetris.Model.Figure;
import tetris.View.Board;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

public class Controller {
    private Board board;
    private int heightBoard;
    private int widthBoard;
    private int blockSize;

    private int RIGHT = 1;

    private Figure figure = new Figure();
    private int field[][];

    private int DELAY = 200;
    private boolean gameOver = false;

    public Controller(final int height, final int width, final int size, Board newBoard){
        heightBoard = height;
        widthBoard = width;
        blockSize = size;
        board = newBoard;
        field = new int[heightBoard+1][widthBoard];
        Arrays.fill(field[height], 1);
    }

    public void start(){
        while(!gameOver){
            try{
                Thread.sleep(DELAY);
            }
            catch (Exception err){
                err.printStackTrace();
            }
            board.repaint();
            checkForFilling();
            if (figureIsFallToGround()){
                figureLeaveOnTheGround();
                figure = new Figure();
                gameOver = figureIsTouchGround();
            }
            else figure.fallDown();
        }
    }

    private boolean figureIsFallToGround(){
       for (Block block : figure.getFigure()) {
            if (field[block.getY() + 1][block.getX()] > 0) return true;
        }
        return false;
    }

    private boolean figureIsTouchGround(){
        for (Block block : figure.getFigure()) {
            if (field[block.getY()][block.getX()] > 0) return true;
        }
        return false;
    }

    private void figureLeaveOnTheGround(){
        for (Block block : figure.getFigure()){
           field[block.getY()][block.getX()] = figure.getColor();
        }
    }

    void checkForFilling(){
        int row = heightBoard - 1;
        while(row > 0){
            int f = 1;
            for (int column = 0; column < widthBoard; column++){
                f *= Integer.signum(field[row][column]);
            }
            if (f > 0) {
                for (int i = row; i > 0; i--) {
                    System.arraycopy(field[i - 1], 0, field[i], 0, widthBoard);
                }
            }
            else row--;
        }
    }

    public void drop(){
        while (!figureIsFallToGround()) figure.fallDown();
    }

    public void move(int direction){
        if (!isTouchWall(direction)){
            if (direction == KeyEvent.VK_LEFT) direction = -1;
            else direction = 1;
            for (Block block: figure.getFigure()) {
                block.setX(block.getX() + direction);
            }
            figure.addX(direction);
        }
    }

    public boolean isTouchWall(final int direction){
        for (Block block : figure.getFigure()){
            if (direction == KeyEvent.VK_LEFT && (block.getX() == 0 || field[block.getY()][block.getX() - 1] > 0)) return true;
            if (direction == KeyEvent.VK_RIGHT && (block.getX() == widthBoard - 1 || field[block.getY()][block.getX() + 1] > 0)) return true;
        }
        return false;
    }

    public boolean isWrongPosition(){
        for (int x = 0; x < figure.getSize(); x++)
            for (int y = 0; y < figure.getSize(); y++)
                if (figure.getValCell(y, x) == 1) {
                    if (y + figure.getY() < 0) return true;
                    if (x + figure.getX() < 0 || x + figure.getX() > widthBoard - 1) return true;
                    if (field[y + figure.getY()][x + figure.getX()] > 0) return true;
                }
        return false;
    }

    public void rotate(){
        figure.rotate(RIGHT);
        if (!isWrongPosition()){
            figure.clear();
            figure.createShape();
        }
        else figure.rotate(0);
    }

    public int getColor(final int x, final int y){
        return field[y][x];
    }

    public boolean isGameOver() { return gameOver; }

    public void paint(Graphics g){
        figure.paint(g);
    }
}
